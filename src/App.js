import {Container} from 'react-bootstrap'
import AppNavBar from './components/AppNavBar';
/*import Banner from './components/Banner';
import Highlights from './components/Highlights';*/
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Courses from './pages/Courses'
import './App.css';

function App() {
  return(
    <>
      <AppNavBar/>
      <Container>
          <Home/>
         <Courses/>
          <Register/>
          <Login/>
      </Container>

    </>
    )
}

export default App;
